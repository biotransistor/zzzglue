"""
# file : gltosinglecell.py
# 
# history: 
#     author: bue
#     date: 2014-10-22
#     license: >= GPL3 
#     language: Python 3.4
#     description: sceening data analysis on the single cell data level 
#
#     biology:endpoint:treatment > biology:endpointset:endpoint:layout:drug:dose:well:object:response > bseluowjr
"""



### txt 2 ### 
# bue 20140811: newstyle
def txtrawj2pyltjrawj ( s_biology, s_endpointset, ts_nucendpoint, ts_cytoendpoint=None, s_layoutfile, s_nucrawfile, s_cytorawfile=None, 
tivtsvivs_layoutvalue=('WellIndex','Well00','Well','Compound','ConcentrationRank','CompoundConcentration_nM'), 
tivtsvivs_nucvalue=('','','','Area','Circularity Factor','Perimeter','Elongation Factor','Center X','X','Center Y','Y'), 
tivtsvivs_cytovalue=None #('','','','Area'), 
ivs_layoutkey='WellIndex',
ivs_rawkeywell='Well',
ivs_rawkeyobject='Object ID',
ts_headrow=('Sample','EndpointSet','Endpoint','Layout','WellIndex','Well00','Well','Compound','Dose_rank','Dose_nM','Object','Measure','Area_nuc','CircularityFactor_nuc','Perimeter_nuc','ElongationFactor_nuc','CenterX_nuc','X_nuc','CenterY_nuc','Y_nuc','Area_cyto','RawDataFile_nuc','RawDataFile_cyto'), 
lt_raw=[]):  # ts_headrow = None
    """
    txtraw2pyltraw - generate python tl raw objects from Scan^R raw output files
    input:
        s_biology : string cell line name
        ts_endpointset : strings to labeling ts_endpoint
        ts_endpoint : tupe of strings to labeling the endpoints, same ordered as tivts_rawdatavalue 
        s_layoutfile : string layout file filename
        s_rawdatafile : string Scan^R raw data file filename
        tivtsvivs_layoutvalue : tuple of or string or interger pointing to values in the s_layoutfile
        tivts_rawdatavalue : tuple of or string or interger pointing to the values in the s_rawdatafile
        ivs_layoutkey : string or integer pointing to the well coordinate column in s_layoutfile
        ivs_rawdatakey : string or interger pointing to the well coordinat column in s_rawdatafile 
        ts_headrow : tuple with column lables
        lt_raw : output list of tuples to be populated
    output:
        lt_raw : populated output list of tuples
    """
