""""
# file: pyglue.py
# 
# history: 
#     autor: bue
#     date: 2014-01-08
#     license: >= GPL3
#     language: Python 3
#
# description: 
#     the glue library is interface betwen tab delimited text files  and python 3 standard object 
#     this particular code is an extension interface between standard python dictionary of dictionary object and numpy arrays 
#
"""

# load library 
#import sys  # error exit
import numpy as np
from pyglue.glue import pydd2yxaxis

##########################
# numpy np array modules #
##########################
def pydd2pya(dd_matrix, dtype_matrix):
    """
    pydd2pya - transform dictionary of dictionary into numpy array  
    input: 
        dd_matrix : dictionary of dictionary
        dtype_matrix : numpty data type
            np.int_ : numpy interger data type 
            np.float_ : numpy float data type  
            np.complex_ : numpy complex number data type
            np.bool_ : numpy boolean data type
            np.string_ : numpy string datatype which stores only one character
            np.object : any python standar library object datatype
            http://docs.scipy.org/doc/numpy/user/basics.types.html
    output: 
        a_matrix : numpy array 
        t_xaxis : python tuple with column labels 
        t_yaxis : python tuple with row labels
    """    
    # input 
    print("i pydd2pya dd_matrix :", dd_matrix)
    print("i pydd2pya dtype_matrix :", dtype_matrix)
    # set output variable 
    a_matrix = None
    t_xaxis = None
    t_yaxis = None
    # get row and columns key
    (t_yaxis, t_xaxis) = pydd2yxaxis(dd_matrix)
    # build empty nd array
    a_matrix = np.zeros(shape=(len(t_yaxis),len(t_xaxis)), dtype=dtype_matrix)
    # fill nd array
    i_y = 0 
    i_x = 0
    for s_y_key in t_yaxis:
        for s_x_key in t_xaxis: 
            value = dd_matrix[s_y_key][s_x_key]
            #print(" y: "+str(i_y)+ " x:"+str(i_x)+ " value:"+str(value))
            a_matrix[i_y,i_x] = value
            i_x += 1
        i_x = 0
        i_y +=1
    i_y = 0
    # output
    print("o pydd2pya a_matrix :\n", a_matrix) 
    print("o pydd2pya t_xaxis :", t_xaxis) 
    print("o pydd2pya t_yaxis :", t_yaxis) 
    return(a_matrix, t_xaxis, t_yaxis)


def pya2pydd(a_matrix, t_xaxis, t_yaxis):
    """
    pya2pydd - transform numpy array into dictionary of dictionary
    input: 
        a_matrix : numpy array 
        t_xaxis : tuple with column labels 
        t_yaxis : tuple with row labels
    output: 
        dd_matrix : dictionary of dictionary
    """
    # input 
    print("i pya2pydd a_matrix :\n", a_matrix) 
    print("i pya2pydd t_xaxis :", t_xaxis) 
    print("i pya2pydd t_yaxis :", t_yaxis) 
    # set output variable
    dd_matrix = {}
    # processing
    i_y = 0
    for a_y in a_matrix: 
        i_x = 0
        for dtype_x in a_y:
            s_value = str(dtype_x) 
            # {t_yaxis(i_y):{t_xaxis(i_x):s_value}}
            try:  # check if for this y key a dictionary exist
                d_matrix = dd_matrix[t_yaxis[i_y]]
            except KeyError:  # bad ending 
                d_matrix = {}
            # store 
            # bue 20140214 : is not yet checking if x key exist, simply overwrites value. 
            d_matrix.update({t_xaxis[i_x]:s_value})  # update row dictionary 
            dd_matrix.update({t_yaxis[i_y]:d_matrix})  # update dictionary of row dictionaries
            i_x += 1
        i_y += 1
    # output
    print("o pya2py dd_matrix :", dd_matrix)
    return(dd_matrix)



####################
# modues self test #
####################
if __name__=='__main__': 
    # pydd2pya
    print('\n*** selftest : py dd 2 py a ***')
    dd_matrix = {'yaxis2':{'xaxis2':'2.2', 'xaxis1':'2.1', 'xaxis3':'2.3'}, 'yaxis1':{'xaxis1':'1.1', 'xaxis2':'1.2', 'xaxis3':'1.3'}, 'yaxis3':{'xaxis1':'3.1', 'xaxis2':'3.2', 'xaxis3':None}}
    pydd2pya(dd_matrix, dtype_matrix=np.float_ ) # np.int_ np.float_ np.complex_ np.bool_ np.string_ np.object

    # pya2pydd
    print('\n*** selftest : py a 2 py dd ***')
    a_matrix =  np.array([[1.1, 1.2, 1.3], [2.1, 2.2, 2.3], [3.1, 3.2, None]], dtype=float)
    pya2pydd(a_matrix, t_xaxis=('xaxis1','xaxis2','xaxis3',), t_yaxis=('yaxis1','yaxis2','yaxis3',))
