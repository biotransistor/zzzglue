""""
# file: gluedd.py
# 
# history: 
#     autor: bue
#     date: 2014-01-08
#     license: >= GPL3
#     language: Python 3
#
# description: 
#     the glue library is interface between simply powerfull tab delimited text files and python 3 standard objects
#     the particular code in this file is the base libarary 
#     check out: glue/man/glueman.pdf
#
"""

# load library 
import sys  # error exit
import csv # read and write files
import re # pattern matching kicking


#####################
# off shore modules #
#####################
def xaxislabelhandle(l_xaxis, tivts_value, tivts_key=None, tivtsvivs_primarykey=None):
    """
    # internal methode
    # handles the label row 
    # form : l_xaxis, tivts_value, tivts_key, tivtsvivs_primarykey 
    # to : ti_xaxis_value, ts_xaxis_value, ti_xaxis_key, ts_xaxis_key, ti_xaxis_primarykey, s_xaxis_primarykey, ti_xaxis, ts_xaxis 
    """
    #### input ####
    print("i xaxislabelhandle l_xaxis :", l_xaxis)
    print("i xaxislabelhandle tivts_value :", tivts_value)
    print("i xaxislabelhandle tivts_key :", tivts_key)
    print("i xaxislabelhandle tivtsvivs_primarykey :", tivtsvivs_primarykey)
    # set empty output variables
    ti_xaxis_value = None 
    ts_xaxis_value = None
    ti_xaxis_key = None
    ts_xaxis_key = None
    ti_xaxis_primarykey = None 
    s_xaxis_primarykey = None
    ti_xaxis = None
    ts_xaxis = None   
    #### handle l_xaxis ####
    if (type(l_xaxis) != list):
        sys.exit("Error: at input xaxislabelhandle, l_xaxis="+str(l_xaxis)+".")
    #### handle tivts_value ### 
    #print("l_xaxis :",l_xaxis)
    # integer case
    if (type(tivts_value[0]) is int):
        ls_value_column = []
        for i in tivts_value: 
            if (type(i) != int): 
                sys.exit("Error: tivts_value is a mix of integer and non integer. This tuple must contain input form the same variable type.")
            ls_value_column.append(l_xaxis[i])
            li_value_column = list(tivts_value)
    # string case
    elif (type(tivts_value[0]) is str):
        li_value_column = [] 
        for s in tivts_value: 
            if (type(s) != str): 
                sys.exit("Error: tivts_value is a mix of string and non string. This tuple must contain input form the same variable type.")
            li_value_column.append(l_xaxis.index(s))
            ls_value_column = list(tivts_value)
    # error case
    else: 
        sys.exit("Error: tivts_value content is neider string nor integer.")
    # output x
    #print("li_value_column :", li_value_column)
    #print("ls_value_column :", ls_value_column)
    li_xaxis = li_value_column
    ls_xaxis = ls_value_column 
    ti_xaxis_value = tuple(li_value_column)
    ts_xaxis_value = tuple(ls_value_column)
    #### handle tivts_key ####
    if (tivts_key != None):  
        # integer case
        if (type(tivts_key[0]) is int):
            ls_key_column = []
            for i in tivts_key: 
                if (type(i) != int): 
                    sys.exit("Error: tivts_key is a mix of integer and non integer. This tupe must contain input form the same variable type.")
                ls_key_column.append(l_xaxis[i])
                li_key_column = list(tivts_key)
        # string case  
        elif (type(tivts_key[0]) is str):
            li_key_column = [] 
            for s in tivts_key: 
                if (type(s) != str): 
                    sys.exit("Error: tivts_key is a mix of string and non string. This tupe must contain input form the same variable type.")
                li_key_column.append(l_xaxis.index(s))
                ls_key_column = list(tivts_key)
        # error case
        else: 
            sys.exit("Error: tivts_key content is neider string nor integer.")
        # output 
        #print("li_key_column :", li_key_column)
        #print("ls_key_column :", ls_key_column)
        li_xaxis = li_key_column + li_xaxis
        ls_xaxis = ls_key_column + ls_xaxis
        ti_xaxis_key = tuple(li_key_column)
        ts_xaxis_key = tuple(ls_key_column)
        ### handle tivtsvivs_primarykey ###
        #print("tivtsvivs_primarykey :",tivtsvivs_primarykey)
        if (tivtsvivs_primarykey != None):
            # tivtsvivs integer or string case ( original: uno dos tre txt +> dd, a, df)
            if (type(tivtsvivs_primarykey) != tuple): 
                # trandform to tuple 
                livtsvivs_primarykey = []
                livtsvivs_primarykey.append(tivtsvivs_primarykey)
                tivtsvivs_primarykey = tuple(livtsvivs_primarykey)
            # tivtsvivs tuple case (original: rdb txt => lt, dt)  
            if (type(tivtsvivs_primarykey) is tuple): 
                # interger case
                if (type(tivtsvivs_primarykey[0]) is int):
                    ls_primarykey_column = []
                    for i in tivtsvivs_primarykey: 
                        if (type(i) != int): 
                            sys.exit("Error: tivtsvivs_primarykey is a mix of integer and non integer. This tupe must contain input form the same variable type.")
                        ls_primarykey_column.append(l_xaxis[i])
                        li_primarykey_column = list(tivtsvivs_primarykey)
                # string case
                elif (type(tivtsvivs_primarykey[0]) is str):
                    li_primarykey_column = []
                    for s in tivtsvivs_primarykey: 
                        if (type(s) != str): 
                            sys.exit("Error: tivtsvivs_primarykey is a mix of string and non string. This tupe must contain input form the same variable type.")
                        li_primarykey_column.append(l_xaxis.index(s))
                        ls_primarykey_column = list(tivtsvivs_primarykey)
                # error case
                else:
                    sys.exit("Error: tivtsvivs_primarykey content is neider string nor integer.")
                # output 
                ti_xaxis_primarykey = tuple(li_primarykey_column)
                #print("li_primarykey_column :", li_primarykey_column)
                #print("ls_primarykey_column :", ls_primarykey_column)
                # column coordinates  
                tivi_primarykey = tuple(li_primarykey_column)
                #print("ti_xaxis_primarykey :", ti_xaxis_primarykey)
                # fuse primiry key column label
                for s in ls_primarykey_column: 
                    if (s_xaxis_primarykey == None):  
                        s_xaxis_primarykey = s
                    else: 
                        s_xaxis_primarykey = s_xaxis_primarykey + "_" + s
                #ls_xaxis.insert(0,s_xaxis_primarykey)  # tuple rdb txt case # bue 2014-06-04 : caused error at txtrdb2pydt, is not compatible with li_xaxis, commented out, hope it caus now error inside glue.   
                #print("s_xaxis_primarykey :", s_xaxis_primarykey)
            # primary keys have to be an elemeny of keys
            b_key = True
            for i_key in tivi_primarykey: 
                if (i_key not in ti_xaxis_key): 
                    b_key = False
            if (not b_key): 
                sys.exit("Error : some tivtsvivs_primarykey elements can not be found in tivts_key "+str(tivtsvivs_primarykey)+", "+str(tivts_key)+".")
    #### get ts_xaxis ####
    #print("li_xaxis :", li_xaxis)
    #print("ls_xaxis :", ls_xaxis)
    ti_xaxis = tuple(li_xaxis)
    ts_xaxis = tuple(ls_xaxis)
    #### output ####
    print("o xaxislabelhandle ti_xaxis_value :", ti_xaxis_value)
    print("o xaxislabelhandle ts_xaxis_value :", ts_xaxis_value)
    print("o xaxislabelhandle ti_xaxis_key :", ti_xaxis_key)
    print("o xaxislabelhandle ts_xaxis_key :", ts_xaxis_key)
    print("o xaxislabelhandle ti_xaxis_primarykey :", ti_xaxis_primarykey)
    print("o xaxislabelhandle s_xaxis_primarykey :", s_xaxis_primarykey)
    print("o xaxislabelhandle ti_xaxis :", ti_xaxis)
    print("o xaxislabelhandle ts_xaxis :", ts_xaxis)
    return(ti_xaxis_value, ts_xaxis_value, ti_xaxis_key, ts_xaxis_key, ti_xaxis_primarykey, s_xaxis_primarykey, ti_xaxis, ts_xaxis) 


def linekicking(ls_line, ls_chr=['"',"'",], ls_str=['^$','^nan$','^NA$','^NaN$',]):  # nan (numpy) NA NaN (excel)
    """
    # internal methde
    # kicks leading and trailing spaces and characte, convert NA and NaN to None
    # from : ls_line
    # to : ls_line
    """
    # input 
    print("i linekicking ls_line :", ls_line)
    # processing
    # kick whitespace
    ls_line = [n.strip() if (type(n) == str) else n for n in ls_line]  # kick leading and trailing spaces 
    # kick charactert
    for s_chr in ls_chr: 
       ls_line = [n.replace(s_chr,'') if (type(n) == str) else n for n in ls_line]  # kick character, for example " double quote and ' quote
    # convert strings 
    for s_str in ls_str:  
        ls_line = [re.sub(s_str,'None',n) if (type(n) == str) else n for n in ls_line]  # substitute by None, for example NA and NaN
    # output 
    print("o linekicking ls_line :", ls_line)
    return(ls_line)



########
# lt 2 #
########

# pylt2extend
# bue: does not make really sense
# bue 2014-05-25: what about append(x), insert(i,x) 

def pylt2pop(lt_matrix, i_pop): 
    """
    pop tuple entry
    input: 
        lt_matrix : list of tuple
        i_pop : integer index points to to the tuple entry which should be popped  
    output: 
        lt_mtrx : popped list of tuple 
    """
    # input
    print("i pylt2pop lt_matrix :", lt_matrix)
    print("i pylt2pop i_pop :", i_pop)
    # set empty output
    lt_mtrx = []
    # process
    for t_matrix in lt_matrix: 
        l_mtrx = list(t_matrix)
        l_mtrx.pop(i_pop)
        t_mtrx = tuple(l_mtrx)
        lt_mtrx.append(t_mtrx)
    # output
    print("o pylt2pop lt_mtrx :", lt_mtrx)
    return(lt_mtrx)



def pylt2txtrdb(s_outputfile, lt_matrix, t_xaxis=None, s_mode='w'):
    """
    pylt2txtrdb - write list of tuple into type rdb text file format 
    input: 
        s_outputfile: string points to output file. without file extension 
        lt_matrix : list of tuple with rdb table content 
        t_xaxis : tuple with the column names. default None
        s_mode : file handel default mode write='w', or append='a', default 'w'
    output: 
        s_outgluefile: txt rdb output file name with extension string
        txt rdb : rdb type text file
    """
    # input
    print("i pylt2txtrdb s_outputfile :", s_outputfile)
    print("i pylt2txtrdb lt_matrix :", lt_matrix)
    print("i pylt2txtrdb t_xaxis :", t_xaxis)
    print("i pylt2txtrdb s_mode :", s_mode)
    # set output variable
    s_outgluefile = s_outputfile+'_glue.txt'
    # open file handle 
    with open(s_outgluefile, s_mode, newline='') as f:
        writer = csv.writer(f, delimiter='\t', quoting=csv.QUOTE_NONE)
        # write x axis (column label row)
        if (t_xaxis != None):
            if (len(t_xaxis) != len(lt_matrix[0])):
                sys.exit("Error: t_xaxis column label length differs from lt_matrix tuple in list length.")
            # manipulate x axis 
            l_xaxis = ['None' if (n == None) else n for n in t_xaxis]
            t_xaxis = tuple(l_xaxis)
            # write x axis
            print('t_xaxis :', str(t_xaxis))
            writer.writerow(t_xaxis)
        # write list of tuple into file 
        for t_y in lt_matrix:
            # manipulate line 
            l_y = ['None' if (n == None) else n for n in t_y]
            t_y = tuple(l_y)
            # write line
            print('y = line :', str(t_y))
            writer.writerow(t_y)
    # output
    print("o pylt2txtrdb s_outgluefile :", s_outgluefile)
    return(s_outgluefile)



########
# dt 2 #
########
def pydt2pylt(dt_matrix, lt_matrix=[]):
    """
    pydt2pylt - converts dictionay of tuple to list of tuple
    input :
        dt_matrix : dictionary of tuple with content
        lt_matrix : list of tuple empty or already with content
    output :
        lt_matrix : list of tuple with all content
    """
    # input
    print("i pydt2pylt dt_matrix :", dt_matrix)
    print("i pydt2pylt lt_matrix :", lt_matrix)
    # processing 
    ls_ykey = list(dt_matrix.keys())
    ls_ykey.sort()
    for s_y in ls_ykey: 
        l_y = []
        l_y.append(s_y)
        l_y.extend(list(dt_matrix[s_y]))
        t_y = tuple(l_y)
        lt_matrix.append(t_y)
    # output
    print("o pydt2pylt lt_matrix :", lt_matrix)
    return(lt_matrix)



def pydt2pydtl(dt_matrix):
    """
    pydt2pydtl - covert dictionary of tuple to ditionary of tuple of lists  
    input: 
        pydt_matrix : dictionary of tuple content
    output: 
        pydtl_matrix : dictionary of tuple of lists content  
    """
    # input
    print("i pydt2pydtl dt_matrix :", dt_matrix)
    # set empty output
    dtl_matrix = {}
    # process 
    for y in dt_matrix.keys():
        t_y = dt_matrix[y]
        ll_row = []
        for x in t_y:
            l_entry = []
            l_entry.append(x)
            ll_row.append(l_entry)
        dtl_matrix.update({y:tuple(ll_row)})
    # output 
    print("o pydt2pydtl dtl_matrix :", dtl_matrix)
    return(dtl_matrix)


# bue 2014-05-25: what about append(x), insert(i,x)
# bue 2014-05-25: what about infix(i,x)  insert in extend manner

def pydt2extend(dt_matrix, dt_extend): 
    """
    extend dictionary of tuple content to dictionary of tuple content
    input: 
        dt_matrix : dictionary of tuple 
        dt_extend : dictionary of tuple extension content
    output: 
        dt_matrix : dt_extend extended dt_matrix 
    """ 
    # input 
    print("i pydt2extend dt_matrix :", dt_matrix)
    print("i pydt2extend dt_extend :", dt_extend)
    # set empty output
    dt_matrixe = {}
    # processing
    ls_key = dt_matrix.keys()
    for s_key in ls_key:
        # get
        try:  
            t_e = dt_extend[s_key] 
            # put 
            t_matrix = dt_matrix[s_key]
            l_matrixe = list(t_matrix)
            l_matrixe.extend(t_e)
            t_matrixe = tuple(l_matrixe)
            dt_matrixe.update({s_key:t_matrixe})
        except:
            pass 
    # output
    print("o pydt2extend dt_matrix :", dt_matrixe)
    return(dt_matrixe)


def pydt2pop(dt_matrix, i_pop): 
    """
    pop tuple entry
    input: 
        dt_matrix : dictionary of tuple  
        i_pop : integer index points to to the tuple entry which should be popped  
    output: 
        dt_mtrx : popped dictionary of tuple 
    """
    # input
    print("i pydt2pop dt_matrix :", dt_matrix)
    print("i pydt2pop i_pop :", i_pop)
    # set empty output
    dt_mtrx = {}
    # process
    ls_key = dt_matrix.keys()
    for s_key in ls_key: 
        t_matrix = dt_matrix[s_key]
        l_mtrx = list(t_matrix)
        l_mtrx.pop(i_pop)
        t_mtrx = tuple(l_mtrx)
        dt_mtrx.update({s_key:t_mtrx})
    # output
    print("o pydt2pop dt_mtrx :", dt_mtrx)
    return(dt_mtrx)


def pydt2txtrdb(s_outputfile, dt_matrix, t_xaxis=None, s_mode='w'):
    """
    pydt2txtrdb - write dictionary of tuple into type rdb text file format, key and tuple 
    input: 
        s_outputfile: string points to output file, without file extension. 
        dt_matrix : dictionary of tuple with rdb table content 
        t_xaxis : tuple with the column names. default None 
        s_mode : file handel mode write='w', append='a'. default 'w'
    output: 
        s_outgluefile: txt rdb output file name with extension
        txt rdb: rdb type text file
    """
    # input 
    print("i pydt2txtrdb s_outputfile :", s_outputfile)
    print("i pydt2txtrdb dt_matrix :", dt_matrix)
    print("i pydt2txtrdb t_xaxis :", t_xaxis)
    print("i pydt2txtrdb s_mode :", s_mode)
    # set output variable
    s_outgluefile = s_outputfile+'_glue.txt'
    # open file handle 
    with open(s_outgluefile, s_mode, newline='') as f:
        writer = csv.writer(f, delimiter='\t', quoting=csv.QUOTE_NONE)
        # extratct y axis 
        l_yaxis = list(dt_matrix.keys())
        l_yaxis.sort()
        t_yaxis = tuple(l_yaxis)
        #print('t_yaxis :', t_yaxis)
        # write x axis (column label row)
        if (t_xaxis != None): 
            if (len(t_xaxis) != (len(dt_matrix[list(dt_matrix.keys())[0]])+1)):
                sys.exit("Error: t_xaxis column label length differs from dt_matrix tuple in dictionary length.")
            # manipulate x axis 
            l_xaxis = ['None' if (n == None) else n for n in t_xaxis]
            t_xaxis = tuple(l_xaxis)
            # write x axis
            print('t_xaxis :', t_xaxis)
            writer.writerow(t_xaxis)
        # write dictioary of tuple into file 
        for x in t_yaxis:
            # put line together
            l_line = [] 
            l_line.append(x) 
            l_line = l_line + list(dt_matrix[x])
            # manipulate line 
            l_line = ['None' if (n == None) else n for n in l_line]
            # write line 
            print('line :', l_line)
            writer.writerow(l_line)
        # output
        print("o pydt2txtrdb s_outgluefile :", s_outgluefile)
    return(s_outgluefile)


########
# dd 2 #
########
def pydd2transpose(dd_matrix): 
    """
    pydd2transpose - transposes dictionary of dictionary  
    input: 
        dd_matrix : dictionary of dictionary with matrix content 
    output: 
        dd_matrix : dictionary of dictionary with transposed matrix content 
    """
    # input
    print("i pydd2transpose dd_matrix :", dd_matrix)
    # output variable empty
    dd_matrixt = {}
    # get the keys out 
    l_y_key = list(dd_matrix.keys())
    t_y_key = tuple(l_y_key)
    l_x_key = list(dd_matrix[list(dd_matrix.keys())[0]].keys())
    t_x_key = tuple(l_x_key)
    #print("t_y_key",t_y_key)
    #print("t_x_key",t_x_key)
    # unpack dictionary of dictionary into dictionary of dictionary 
    for y in t_y_key: 
        for x in t_x_key: 
            value = dd_matrix[y][x]
            #print("y, x, value :", y, x, value)
            # {s_yaxis_key:{s_xaxis_key:value}}
            try:  # check for this row_key a row dictionary exist
                d_matrixt = dd_matrixt[x]
            except KeyError:  # bad ending 
                d_matrixt = {}
            # store 
            d_matrixt.update({y:value})  # update row dictionary 
            dd_matrixt.update({x:d_matrixt})  # update dictionary of row dictionaries
    # output
    print("o pydd2transpose dd_matrix :", dd_matrixt)
    return(dd_matrixt)



def pydd2yxaxis(dd_matrix):
    """ 
    pydd2yxaxis - extract tuple of x axis keys and tuple of y axis keys from dictionary of dictionary 
    input: 
        dd_matrix : dictionary of dictionary with matrix content 
    output: 
        t_yaxis : tuple of sorted y axis labels
        t_xaxis : tuple of sorted x axis labels 
    """
    # input
    print("i pydd2yxaxis dd_matrix :", dd_matrix)
    # set output variable 
    t_xaxis = None
    t_yaxis = None   
    # get row and columns key and order them alphabeticaly
    l_ykey = list(dd_matrix.keys())
    l_ykey.sort()
    t_yaxis = tuple(l_ykey)
    l_xkey = list(dd_matrix[list(dd_matrix.keys())[0]].keys())
    l_xkey.sort()
    t_xaxis = tuple(l_xkey)
    # output
    print("o pydd2yxaxis t_yaxis :", t_yaxis)    
    print("o pydd2yxaxis t_xaxis :", t_xaxis)    
    return(t_yaxis, t_xaxis)



def pydd2pydtrdb(dd_matrix, dt_matrix={}):
    """
    pydd2pydtrdb - converts dictionay of dictionary into dictionary tuple in rdb format
    input :
        dd_matrix : dictionary of dictionary
        dt_matrix : dictionary of tuple
    output :
        dt_matrix : dictionary of tuple
    """
    # input 
    print("i pydd2pydtrdb dd_matrix :", dd_matrix)
    print("i pydd2pydtrdb dt_matrix :", dt_matrix)
    # get keys
    (t_yaxis, t_xaxis) = pydd2yxaxis(dd_matrix)
    # dd pull for each row 
    for s_y_key in t_yaxis:
        try: 
            t_value = dt_matrix[s_y_key]
            sys.exit("Error: dd_matrix can not be converted in to dt_matrix, dt_matrix already has as an entry for key "+s_y_key+".")
        except:
            d_matrix = (dd_matrix[s_y_key])
            # for each entry in the row
            for s_x_key in t_xaxis: 
                s_key = s_y_key +'_'+s_x_key
                t_value = (s_y_key,s_x_key,d_matrix[s_x_key])
                # dt push
                dt_matrix.update({s_key:t_value})
    # output
    print("o pydd2pydtrdb dt_matrix :", dt_matrix)
    return(dt_matrix)



def pydd2pydtspre(dd_matrix, dt_matrix={}):
    """
    pydd2pydtspre - converts dictionay of dictionary intpo dictionary tuple in spre format 
    input :
        dd_matrix : dictionary of dictionary
        dt_matrix : dictionary of tuple
    output :
        dt_matrix : dictionary of tuple
    """
    # input 
    print("i pydd2pydtspre dd_matrix :", dd_matrix)
    print("i pydd2pydtspre dt_matrix :", dt_matrix)
    # get keys
    (t_yaxis, t_xaxis) = pydd2yxaxis(dd_matrix)
    # dd pull for each row 
    for s_y_key in t_yaxis:
        try: 
            t_value = dt_matrix[s_y_key]
            sys.exit("Error: dd_matrix can not be converted in to dt_matrix, dt_matrix already has as an entry for key "+s_y_key+".")
        except:
            l_value = [] 
            d_matrix = (dd_matrix[s_y_key])
            # for each entry in the row
            for s_x_key in t_xaxis: 
                l_value.append(d_matrix[s_x_key])
            t_value = tuple(l_value)
            # dt push 
            dt_matrix.update({s_y_key:t_value})
    # output
    print("o pydd2pydtspre dt_matrix :", dt_matrix)
    return(dt_matrix)



def pydd2txtrdb(s_outputfile, dd_matrix, dd_xaxis=None, dd_yaxis=None):
    """
    pydd2txtrdb - write dictionary of dictionary into type rdb text file format 
    input: 
        s_outputfile : string points to output file. without file extension 
        dd_matrix : dictionary of dictionary with matrix content
        dd_xaxis : dictionary of dictionary with column annotation. default None
        dd_yaxis : dictionary of dictionary with row annotation. default None
    output: 
        s_outgluefile : txt rdb output file name with file extension
        txt : rdb type text file
    Note: 
    dd_xaxis and dd_yaxis can both be None. In this case is just dd_matrix writen out. 
    """
    # input
    print("i pydd2txtspre s_outputfile : ", s_outputfile)
    print("i pydd2txtspre dd_matrix : ", dd_matrix)
    print("i pydd2txtspre dd_xaxis : ", dd_xaxis)
    print("i pydd2txtspre dd_yaxis :", dd_yaxis)
    # empty output 
    s_outgluefile = None
    l_head = []
    # get matrix axis 
    (t_matrix_yaxis, t_matrix_xaxis) = pydd2yxaxis(dd_matrix)
    # get y axis 
    l_head.append('y_track')
    if (dd_yaxis != None):
        (t_y_yaxis, t_y_xaxis) = pydd2yxaxis(dd_yaxis)
        l_head.extend(list(t_y_xaxis))
    # get x axis  
    l_head.append('x_track')
    if (dd_xaxis != None):
        dd_xaxistrans = pydd2transpose(dd_xaxis)
        dd_xaxis = {}
        for s_key in t_matrix_xaxis: 
            dd_xaxis.update({s_key : dd_xaxistrans[s_key]})
        dd_xaxis = pydd2transpose(dd_xaxis)
        (t_x_yaxis, t_x_xaxis) = pydd2yxaxis(dd_xaxis)
        l_head.extend(list(t_y_xaxis))
    # get list of tuples
    l_head.append('value')
    lt_matrix = [tuple(l_head)] 
    for y_key in t_matrix_yaxis: 
        # get tuple
        for x_key in t_matrix_xaxis:
            l_line = []
            #print("l_line :", l_line)
            # y part 
            l_line.append(y_key)  # ytrack
            #print("l_line :", l_line)
            if (dd_yaxis != None): 
                for y_xaxis in t_y_xaxis: 
                    l_line.append(dd_yaxis[y_key][y_xaxis])
                    #print("l_line :", l_line)
            # x part
            l_line.append(x_key)  # xtrack
            #print("l_line :", l_line)
            if (dd_xaxis != None):   
                for x_yaxis in t_x_yaxis:
                    l_line.append(dd_xaxis[x_yaxis][x_key])
                    #print("l_line :", l_line)
            # matrix part
            l_line.append(dd_matrix[y_key][x_key])
            #print("l_line :", l_line)
            # get list
            lt_matrix.append(tuple(l_line))
            #print("lt_matrix :", lt_matrix)
    # output
    s_outgluefile = pylt2txtrdb(s_outputfile, lt_matrix=lt_matrix, t_xaxis=None, s_mode='w')
    print("o pydd2txtspre s_outgluefile:", s_outgluefile)
    return(s_outgluefile)



def pydd2txtspre(s_outputfile, dd_matrix, dd_xaxis=None, dd_yaxis=None): 
    """
    pydd2txtspre - write dictionary of dictionary into type spre text file format 
    input: 
        s_outputfile : string points to output file. without file extension 
        dd_matrix : dictionary of dictionary with matrix content
        dd_xaxis : dictionary of dictionary with column annotation. default None
        dd_yaxis : dictionary of dictionary with row annotation. default None
    output: 
        s_outgluefile : txt spre output file name with extension
        txt : spre type text file
    Note: 
    dd_xaxis and dd_yaxis can both be None. In this case is just dd_matrix writen out. 
    """
    # input
    print("i pydd2txtspre s_outputfile :", s_outputfile)
    print("i pydd2txtspre dd_matrix :", dd_matrix)
    print("i pydd2txtspre dd_xaxis :", dd_xaxis)
    print("i pydd2txtspre dd_yaxis :", dd_yaxis)
    # empty output 
    s_outgluefile = False 
    # get matrix axis 
    (t_matrix_yaxis, t_matrix_xaxis) = pydd2yxaxis(dd_matrix)
    # get y axis
    if (dd_yaxis != None):
        (t_y_yaxis, t_y_xaxis) = pydd2yxaxis(dd_yaxis)
    # get x axis 
    if (dd_xaxis != None):
        dd_xaxistrans = pydd2transpose(dd_xaxis)
        dd_xaxis = {}
        for s_key in t_matrix_xaxis: 
            dd_xaxis.update({s_key : dd_xaxistrans[s_key]})
        dd_xaxis = pydd2transpose(dd_xaxis)
        (t_x_yaxis, t_x_xaxis) = pydd2yxaxis(dd_xaxis)
    # x part
    if (dd_xaxis != None):
        # get dt x
        dt_xaxis =  pydd2pydtspre(dd_xaxis)
        # build row label dictionary of tuple
        dt_x_ykey = {}
        for s_key in t_x_yaxis: 
            dt_x_ykey.update({s_key:(s_key,)})
        # fuse row label and dt x
        dt_xaxis = pydt2extend(dt_x_ykey, dt_xaxis)
        if (dd_yaxis != None):
            # build spacer tuple
            l_space = []
            for n in range(len(t_y_xaxis)): 
                l_space.append('')
            # build spacer dictionary of tuple
            dt_space = {} 
            for s_key in t_x_yaxis: 
                dt_space.update({s_key:l_space})
            # fuse space and dt x
            dt_xaxis = pydt2extend(dt_space, dt_xaxis)
        # data dt 2 lt
        lt_xaxis = pydt2pylt(dt_xaxis)
        lt_xaxis = pylt2pop(lt_xaxis, i_pop=0)
        # label row dt 2 lt
        l_matrix_xaxis = list(t_y_xaxis)
        #for n in range(len(t_y_xaxis)): 
        #    l_matrix_xaxis.append('')
        l_matrix_xaxis.append('tracks')
        l_matrix_xaxis.extend(list(t_matrix_xaxis))
        lt_xaxis.append(tuple(l_matrix_xaxis))        
    else: 
        # label row t 2 lt
        l_matrix_xaxis = ['tracks',]
        l_matrix_xaxis.extend(list(t_matrix_xaxis))
        lt_xaxis = [tuple(l_matrix_xaxis),]
    # y part 
    if (dd_yaxis != None): 
        dt_y = pydd2pydtspre(dd_matrix=dd_yaxis, dt_matrix={})
        # matrix part 
        dt_matrix = pydd2pydtspre(dd_matrix=dd_matrix, dt_matrix={})
        # save matrix keys 
        dt_key = {}
        for s_key in t_matrix_yaxis:
            dt_key.update({s_key:(s_key,)})
        dt_matrix = pydt2extend(dt_matrix=dt_key, dt_extend=dt_matrix)
        # fuse y and matrix
        dt_ymatrix = pydt2extend(dt_matrix=dt_y, dt_extend=dt_matrix)
        lt_ymatrix = pydt2pylt(dt_matrix=dt_ymatrix, lt_matrix=[])
        lt_ymatrix = pylt2pop(lt_matrix=lt_ymatrix,i_pop=0)
    else: 
        # matrix part 
        dt_matrix = pydd2pydtspre(dd_matrix=dd_matrix, dt_matrix={})
        lt_ymatrix = pydt2pylt(dt_matrix=dt_matrix, lt_matrix=[])
    # output
    s_outgluefile = pylt2txtrdb(s_outputfile, lt_matrix=lt_xaxis, t_xaxis=None, s_mode='w')
    s_outgluefile = pylt2txtrdb(s_outputfile, lt_matrix=lt_ymatrix, t_xaxis=None, s_mode='a')
    print("o pydd2txtspre s_outgluefile :", s_outgluefile)
    return(s_outgluefile)


##########
# spre 2 #
##########
def spre2dd(s_matrix, tivtsvivs_value, i_ycoordinate, i_xcoordinate) : 
    """
    # internal methode
    # from : s_matrix, tivtsvivs_value, i_ycoordinate, i_xcoordinate
    # to : dd_matrix
    """
    # input
    print("i spre2dd s_matrix :", s_matrix)
    print("i spre2dd tivtsvivs_value :", tivtsvivs_value)
    print("i spre2dd i_ycoordinate :", i_ycoordinate)
    print("i spre2dd i_xcoordinate :", i_xcoordinate)
    # set empty output variables 
    dd_matrix = {}
    # handle tivtsvivs_value input 
    if (type(tivtsvivs_value) != tuple):
        tivtsvivs_value = (tivtsvivs_value,)
    # read out inputfile 
    with open(s_matrix, newline='') as f:
        reader = csv.reader(f, delimiter='\t', quoting=csv.QUOTE_NONE)
        for line in reader:
            # handel line 
            #print("line :",  str(reader.line_num), str(line))
            line = linekicking(line)
            if (set(line) != {'None'}):  # kick empty rows
                if ( line != ['None'] ): # kick empty rows
                    # handle label row 
                    if ((reader.line_num - 1) == i_ycoordinate):  # reader.line_num start counting by 1 not by 0. 
                        l_xaxis_label = line
                        (ti_xaxis_value, ts_xaxis_value, ti_xaxis_key, ts_xaxis_key, ti_xaxis_primarykey, s_xaxis_primarykey, ti_xaxis, ts_xaxis) = xaxislabelhandle(l_xaxis=line, tivts_value=tivtsvivs_value, tivts_key=(i_xcoordinate,), tivtsvivs_primarykey=i_xcoordinate)
                    # data row
                    elif ((reader.line_num - 1) > i_ycoordinate):  # reader.line_num start counting by 1 not by 0.
                        s_yaxis_key = line[ti_xaxis_primarykey[0]]  # extract y keys, ti_xaxis_primarykey will be 1 integer only but in a tuple, because input was 1 integer or string only 
                        for i in ti_xaxis_value: 
                            s_xaxis_key = l_xaxis_label[i]  # extract x key 
                            value = line[i]  # extract value
                            #print("y_key, x_key, value :", s_yaxis_key, s_xaxis_key, value) 
                            # entry dictionary of dictionary
                            # {s_yaxis_key:{s_xaxis_key:value}}
                            try:  # check if for this row_key a row dictionary exist
                                d_matrix = dd_matrix[s_yaxis_key]
                            except KeyError:  # bad ending 
                                d_matrix = {}
                            # check if for this column_key in this row already a entry exist 
                            try: 
                                entry = d_matrix[s_xaxis_key]
                                sys.exit("Error: xy coordinate "+s_yaxis_key +", "+s_xaxis_key+" exist twice in this input. can not build dictionary of dictionary.")
                            except:   
                                d_matrix.update({s_xaxis_key:value})  # update row dictionary 
                                dd_matrix.update({s_yaxis_key:d_matrix})  # update dictionary of row dictionaries
                        # else some leading row to be ignored 
    f.close()
    # output 
    print("o spre2dd dd_matrix :", dd_matrix)
    return(dd_matrix)


def txtspre2pydd(s_matrix, tivs_yxaxis=(0,0)):
    """
    txtspre2pydd - load txt file in spread sheet format into dictionary of dictionary  
    input: 
        s_matrix : string points to spre input file 
        tivs_yxaxis : string or tuple of two integer points to row and column holding the row and column labels. default (0,0,) or 'tracks'
    output: 
        dd_matrix : dictionary of dictionary with matrix content 
        dd_xaxis : dictionary of dictionary 
        dd_yaxis : dictionary of dictionary 
    """
    # input
    print("i txtspre2pydd s_matrix", s_matrix)
    print("i txtspre2pydd tivs_yxaxis", tivs_yxaxis)
    # set empty output variables
    dd_matrix = None
    dd_xaxis = None
    dd_yaxis = None
    # get value coordinates
    i_linelength = None
    i_ycoordinate = None
    i_xcoordinate = None
    s_yxaxis = None
    with open(s_matrix, newline='') as f:
        reader = csv.reader(f, delimiter='\t', quoting=csv.QUOTE_NONE) # read out inputfile
        # tivs_yxaxis string case 
        if (type(tivs_yxaxis) == str):
            for line in reader:
                #print("line :", str(reader.line_num), str(line))
                # handel line 
                line = linekicking(line)
                # find column label row 
                try:  
                    i_xcoordinate = line.index(tivs_yxaxis)
                    i_ycoordinate = reader.line_num - 1
                    i_linelength = len(line)
                    s_yxaxis = tivs_yxaxis 
                    break 
                except:
                    pass  # nop 
        # tivs_yxaxis integer case 
        else:
            i_ycoordinate = tivs_yxaxis[0]
            i_xcoordinate = tivs_yxaxis[1]
            for line in reader: 
                #print("line :", str(reader.line_num), str(line))
                # find column label row
                if ((reader.line_num - 1) == i_ycoordinate):
                    # hanle line  
                    line = linekicking(line)
                    i_linelength = len(line)
                    s_yxaxis = line[i_xcoordinate]
                    break
                # else nop 
    f.close()
    # BUE 20140829: bugs found! (0,1) (1,0) crashes!(0,0) (1,1) ok!
    # BUE 20140930: bug killed. 
    # set value coordinates
    tivi_value_matrix = tuple(range((i_xcoordinate + 1),i_linelength))
    tivi_value_yaxis = tuple(range(0,i_xcoordinate))
    # read out input file
    # matrix part
    dd_matrix = spre2dd(s_matrix=s_matrix, tivtsvivs_value=tivi_value_matrix, i_ycoordinate=i_ycoordinate, i_xcoordinate=i_xcoordinate) 
    (t_matrix_yaxis, t_matrix_xaxis)=pydd2yxaxis(dd_matrix)
    # y part
    if (i_xcoordinate == 0):
        dd_yaxis = None
    else:
        dd_yaxis = spre2dd(s_matrix=s_matrix, tivtsvivs_value=tivi_value_yaxis, i_ycoordinate=i_ycoordinate, i_xcoordinate=i_xcoordinate)  
    # x part 
    if (i_ycoordinate == 0):
        dd_xaxis = None
    else: 
        # fetch x part and matrix into dt_xaxis 
        (dt_xaxis, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = txtrdb2pydt(s_matrix=s_matrix, tivtsvivs_value=tivi_value_matrix, tivtsvivs_key=(i_xcoordinate,), tivtsvivs_primarykey=(i_xcoordinate,), ivs_labelrow=None, b_dtl=False) 
        dt_xaxis = pydt2pop(dt_xaxis, i_pop=0)
        # populate dd_xaxis out of dt_xaxis
        dd_x = {}  
        for s_ykey in dt_xaxis.keys(): 
            # jump over label row and matrix rows 
            ls_jumper = list(t_matrix_yaxis)
            ls_jumper.append(s_yxaxis) 
            if ( s_ykey not in ls_jumper ):  
                # get data 
                d_x = {}
                for i_xkey in range(len(t_matrix_xaxis)):
                    s_xkey = t_matrix_xaxis[i_xkey]
                    d_x.update({s_xkey : dt_xaxis[s_ykey][i_xkey]})
                dd_x.update({s_ykey : d_x})
        # put data 
        dd_xaxis = pydd2transpose(dd_x)
    # output
    print("o txtspre2pydd dd_matrix", dd_matrix)
    print("o txtspre2pydd dd_yaxis", dd_yaxis)
    print("o txtspre2pydd dd_xaxis", dd_xaxis)
    return(dd_matrix, dd_yaxis, dd_xaxis)



#########
# rdb 2 #
#########
def txtrdb2pylt(s_matrix, tivtsvivs_value, tivtsvivs_key=None, ivs_labelrow=None):
    """
    txtrdb2pylt - load txt file in rdb format into list of tuple
    input: 
        s_matrix : string points to rdb input file 
        tivtsvivs_value : tuple of string or integer points to column holding the values. first column is 0
        tivtsvivs_key : tuple of string or integer points to column holding the keys. first colum ins 0. default None is no key
        (tivtsvivs_primarykey=None !) 
        ivs_labelrow : integer or string or None. first row is 0. default None is no label row 
    output: 
        lt_matrix : list of tuple
        ts_xaxis : tuple with all column labels 
        ts_xaxis_value : tuple with value columns labels 
        ts_xaxis_key : tuple with key columns labels 
    Note: 
    columns neider marked as tivtsvivs_value nor tivtsvivs_key will be skiped! 
    """
    # input 
    print("i txtrdb2lt s_matrix :", s_matrix)
    print("i txtrdb2lt tivtsvivs_value :", tivtsvivs_value) 
    print("i txtrdb2lt tivtsvivs_key :", tivtsvivs_key) 
    print("i txtrdb2lt tivtsvivs_primarykey = None")
    print("i txtrdb2lt ivs_labelrow :", ivs_labelrow)
    # set empty output
    lt_matrix = []
    ts_xaxis = None
    ts_xaxis_value = None
    ts_xaxis_key = None
    # handle tivtsvivs_value input 
    if (type(tivtsvivs_value) != tuple):
        tivtsvivs_value = (tivtsvivs_value,)
    # handle tivtsvivs_key input 
    if (tivtsvivs_key != None):  # x axis
        if (type(tivtsvivs_key) != tuple):
            tivtsvivs_key = (tivtsvivs_key,)
    # ivs_labelrow == None case
    if (ivs_labelrow == None):
        # set ivs_labelrow
        ivs_labelrow = -1
        # set ti_xaxis
        if (tivtsvivs_key == None):
            li_xaxis = list(tivtsvivs_value)
        else:
            li_xaxis = list(tivtsvivs_key)
            li_xaxis.extend(tivtsvivs_value)
        ti_xaxis = tuple(li_xaxis)
    # read out input file
    with open(s_matrix, newline='') as f:
        reader = csv.reader(f, delimiter='\t', quoting=csv.QUOTE_NONE)
        for line in reader:
            #print("file read out :", str(reader.line_num), str(line))
            line = linekicking(line)
            if (set(line) != {'None'}):  # kick empty rows
                if ( line != ['None'] ): # kick empty rows
                    # find column label row if ivs_labelrow is string
                    if (type(ivs_labelrow) == str):
                       try:  
                           line.index(ivs_labelrow) 
                           ivs_labelrow = reader.line_num - 1  # reader.line_num start counting by 1 not by 0
                       except:
                            pass  # nop 
                    # find column label row if ivs_labelrow is integer
                    if (type(ivs_labelrow) == int):
                        if ((reader.line_num - 1) == ivs_labelrow):  # reader.line_num start counting by 1 not by 0
                            # handle label row  
                            (ti_xaxis_value, ts_xaxis_value, ti_xaxis_key, ts_xaxis_key, ti_xaxis_primarykey, s_xaxis_primarykey, ti_xaxis, ts_xaxis) = xaxislabelhandle(l_xaxis=line, tivts_value=tivtsvivs_value, tivts_key=tivtsvivs_key, tivtsvivs_primarykey=None)
                        # handle data row 
                        if ((reader.line_num - 1) > ivs_labelrow):
                            # get tuple
                            l_row = []
                            for i in ti_xaxis: 
                                l_row.append(line[i])
                            t_row = tuple(l_row)
                            # attach row
                            lt_matrix.append(t_row)
                            #print(t_row)
                    # else some leading row to be ignored 
    f.close()
    # output 
    print("o txtrdb2lt lt_matrix :", lt_matrix)
    print("o txtrdb2lt ts_xaxis :", ts_xaxis)
    print("o txtrdb2lt ts_xaxis_value :", ts_xaxis_value)
    print("o txtrdb2lt ts_xaxis_key :", ts_xaxis_key)
    return(lt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key)



def rdb2dt(s_matrix, tivtsvivs_value, tivtsvivs_key, tivtsvivs_primarykey, ivs_labelrow=None, b_dtl=True):
    """
    # internal method
    # if b_dtl flag set, dictionatry of tuple list output, multiple values per primarykey possible, else only one value per primarykey possible 
    # from : s_matrix, tivtsvivs_value, tivtsvivs_key, tivtsvivs_primarykey, ivs_labelrow, b_dtl
    # to : dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey
    """
    # dt dictionary of tuple case
    #input 
    print("i rdb2dt s_matrix :", s_matrix)
    print("i rdb2dt tivtsvivs_value :", tivtsvivs_value)
    print("i rdb2dt tivtsvivs_key:", tivtsvivs_key)
    print("i rdb2dt tivtsvivs_primarykey :", tivtsvivs_primarykey)
    print("i rdb2dt ivs_labelrow :", ivs_labelrow)
    print("i rdb2dt b_dtl :", b_dtl)
    # set empty output
    ts_xaxis = None
    ts_xaxis_value = None
    ts_xaxis_key = None
    s_xaxis_primarykey = None
    dt_matrix = {}
    # handle tivtsvivs_value input 
    if (type(tivtsvivs_value) != tuple):
        tivtsvivs_value = (tivtsvivs_value,)
    # handle tivtsvivs_key input    
    if (type(tivtsvivs_key) != tuple):
        tivtsvivs_key = (tivtsvivs_key,)
    # ivs_labelrow == None case, 
    # in ivs_labelrow is None case tivtsvivs_primarykey, tivtsvivs_key, tivtsvivs_value, can only be integer. None no alowed
    if (ivs_labelrow == None):
        # set ivs_labelrow
        ivs_labelrow = -1
        # set ti_xaxis 
        if (tivtsvivs_key[0] == None):
            li_xaxis = list(tivtsvivs_value)
        else:
            li_xaxis = list(tivtsvivs_key)
            li_xaxis.extend(tivtsvivs_value)
        ti_xaxis = tuple(li_xaxis)
        # set ti_xaxis_primarykey 
        if (type(tivtsvivs_key) != tuple):
            ti_xaxis_primarykey = (tivtsvivs_primarykey,)
        else:
            ti_xaxis_primarykey = tivtsvivs_primarykey
        # primary keys have to be an elemeny of keys 
        b_key = True
        for i_key in ti_xaxis_primarykey: 
            if (i_key not in tivtsvivs_key): 
                    b_key = False
            if (not b_key): 
                sys.exit("Error : some tivtsvivs_primarykey elements can not be found in tivtsvivs_key " + str(tivtsvivs_primarykey) +", "+ str(tivtsvivs_key)+".")
    # read out input file
    with open(s_matrix, newline='') as f:
        reader = csv.reader(f, delimiter='\t', quoting=csv.QUOTE_NONE)
        for line in reader:
            #print("file read out :", str(reader.line_num), str(line))
            line = linekicking(line)
            if ( set(line) != {'None'} ): # kick empty rows
                if ( line != ['None'] ): # kick empty rows
                    # find column label row if ivs_labelrow is string
                    if (type(ivs_labelrow) == str):
                       try:  
                           line.index(ivs_labelrow)  # is string in this line?  
                           ivs_labelrow = reader.line_num - 1  # reader.line_num start counting by 1 not by 0
                       except ValueError:
                            pass  # nop 
                    # find column label row if ivs_labelrow is integer
                    if (type(ivs_labelrow) == int):
                        if ((reader.line_num - 1) == ivs_labelrow):  # reader.line_num start counting by 1 not by 0
                            # handle label row  
                            (ti_xaxis_value, ts_xaxis_value, ti_xaxis_key, ts_xaxis_key, ti_xaxis_primarykey, s_xaxis_primarykey, ti_xaxis, ts_xaxis) = xaxislabelhandle(l_xaxis=line, tivts_value=tivtsvivs_value, tivts_key=tivtsvivs_key, tivtsvivs_primarykey=tivtsvivs_primarykey)
                        # handle data row 
                        if ((reader.line_num - 1) > ivs_labelrow):
                            # get tuple
                            l_row = []
                            for i in ti_xaxis: 
                                l_row.append(line[i])
                            t_row = tuple(l_row)
                            # fuse primary key
                            s_primarykey = None
                            for i in ti_xaxis_primarykey:
                                s = line[i]
                                #print(str(ti_xaxis_primarykey), str(i))
                                #print(str(line), s)
                                if (s_primarykey == None):
                                    s_primarykey = s
                                else: 
                                    s_primarykey = s_primarykey + '_' + s
                            # entry dictionary of tuple
                            # check if for this primary key already a tuple entry exist
                            try:    
                                t_matrix = dt_matrix[s_primarykey]
                                # b_dtl = True case; append row
                                if (b_dtl): 
                                    step = 0
                                    for x in t_row:
                                        b_w = True # set write flag true
                                        for n in dt_matrix[s_primarykey][step]:
                                            if (n == x):
                                                b_w = False  # set write falg false if value alrady in list
                                                break # jump out of for loop
                                        if (b_w): # write if writeflag true
                                            dt_matrix[s_primarykey][step].append(x)
                                        step += 1 
                                # b_dtl = False case; error  
                                else: 
                                    sys.exit("Error: at rdb2dt, b_dtl=False set, but your tivtsvivs_primarykey setting (or s_xaxis_primarykey) is not unique at "+s_primarykey+".")
                            # bad ending
                            except KeyError: 
                                #  b_dtl = True case
                                if (b_dtl):
                                    # convert t to tl
                                    ll_row = []
                                    for x in t_row: 
                                        l_entry = [] 
                                        l_entry.append(x)
                                        ll_row.append(l_entry)
                                    t_row = tuple(ll_row)
                                # b_dtl = any case; update dictionary of tuple
                                dt_matrix.update({s_primarykey:t_row})
                    # else some leading row to be ignored 
    f.close()
    #input 
    print("o rdb2dt dt_matrix :", dt_matrix)
    print("o rdb2dt ts_xaxis :", ts_xaxis)
    print("o rdb2dt ts_xaxis_value :", ts_xaxis_value)
    print("o rdb2dt ts_xaxis_key :", ts_xaxis_key)
    print("o rdb2dt s_xaxis_primarykey :", s_xaxis_primarykey)
    return(dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey)



def txtrdb2pydt(s_matrix, tivtsvivs_value, tivtsvivs_key, tivtsvivs_primarykey, ivs_labelrow=None, b_dtl=False):
    """
    txtrdb2pydt - load txt file in rdb format into dictionary of tuple 
    input: 
        s_matrix : string points to rdb input file. label row have to exist!
        tivtsvivs_value : tuple of string or integer points to column holding the values. 0 is the first column
        tivtsvivs_key : tuple of string or integer points to column holding the keys. 0 is the first column
        tivtsvivs_primarykey : tuple of string or integer points to the key columns which represent the primary key. tivtsvivs_primarykey != None. 0 is the first column 
        ivs_labelrow : integer or string. default None is no label row. 0 is the first row
        b_dtl: boolean. default False 
    output: 
        dt_matrix : dictionary of tuple
        dt_matrix_count : dictionary of tuple containing counts of input filed per output filed if b_dtl, else None. 
        ts_xaxis : tuple with all column labels 
        ts_xaxis_value : tuple with value columns labels 
        ts_xaxis_key : tuple with key columns labels 
        s_xaxis_primarykey : string with primary key column label
        ts_yaxis : tuple with primarykey row labels
    Note: 
    columns neider marked as tivtsvivs_value nor tivtsvivs_key will be skiped! 
    """
    print("i txtrdb2pydt s_matrix :", s_matrix)
    print("i txtrdb2pydt tivtsvivs_value :", tivtsvivs_value)
    print("i txtrdb2pydt tivtsvivs_key :", tivtsvivs_key)
    print("i txtrdb2pydt tivtsvivs_primarykey :", tivtsvivs_primarykey)
    print("i txtrdb2pydt ivs_labelrow : ", ivs_labelrow)
    print("i txtrdb2pydt b_dtl :", b_dtl)
    # set empty output
    ts_xaxis = None
    ts_xaxis_value = None
    ts_xaxis_key = None
    s_xaxis_primarykey = None
    ts_yaxis = None
    dt_matrix = None
    dt_matrix_count = None
    # read out input file
    (dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey) = rdb2dt(s_matrix=s_matrix, tivtsvivs_value=tivtsvivs_value, tivtsvivs_key=tivtsvivs_key, tivtsvivs_primarykey=tivtsvivs_primarykey, ivs_labelrow=ivs_labelrow, b_dtl=b_dtl)
    # get y axis
    l_yaxis = list(dt_matrix.keys())
    l_yaxis.sort()
    ts_yaxis = tuple(l_yaxis)
    # get dt_matrix_count
    if (b_dtl): 
        dt_matrix_count = {}
        for y in dt_matrix:
            l_entry = []
            t_y = dt_matrix[y]
            for x in t_y: 
                # bue 20140930: with proper linekicking 'None' should be the only option. (x == [None]) or (x == ['']) not needed. 
                if ( x == ['None'] ): # handle empty cell, 
                    x = []
                l_entry.append(len(x))
            dt_matrix_count.update({y:tuple(l_entry)})
    # output
    print("o txtrdb2pydt dt_matrix :", dt_matrix)
    print("o txtrdb2pydt ts_xaxis :", ts_xaxis)
    print("o txtrdb2pydt ts_xaxis_value :", ts_xaxis_value)
    print("o txtrdb2pydt ts_xaxis_key :", ts_xaxis_key)
    print("o txtrdb2pydt s_xaxis_primarykey :", s_xaxis_primarykey)
    print("o txtrdb2pydt ts_yaxis :", ts_yaxis)
    print("o txtrdb2pydt dt_matrix_count :", dt_matrix_count)
    return(dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count)



def rdb2dd(s_matrix, i_yaxis, i_xaxis, i_value, i_labelrow=None):
    """
    # internal methde - load txt file in rdb format into dictionary of dictionary
    # from : s_matrix, i_yaxis, i_xaxis, i_value, i_labelrow
    # to : dd_matrix
    """
    # input 
    print("i rdb2dd s_matrix :", s_matrix)
    print("i rdb2dd i_yaxis :", i_yaxis)            
    print("i rdb2dd i_xaxis :", i_xaxis)            
    print("i rdb2dd i_value :", i_value)            
    print("i rdb2dd i_labelrow :", i_labelrow)            
    # empyt output 
    dd_matrix = {}
    # read out inputfile 
    if (i_labelrow == None):
        i_labelrow = -1
    with open(s_matrix, newline='') as f:
        reader = csv.reader(f, delimiter='\t', quoting=csv.QUOTE_NONE)
        for line in reader:
            # handle line
            #print("line :",  str(reader.line_num), str(line))
            line = linekicking(line)
            if (set(line) != {'None'}):  # kick empty rows
                if ( line != ['None'] ): # kick empty rows
                    # handle data row 
                    if ((reader.line_num - 1) > i_labelrow):  # reader.line_num start counting by 1 not by 0.
                        # check if for this primary key already a tuple entry exist
                        try: 
                            d_matrix = dd_matrix[line[i_yaxis]]
                        except: 
                            d_matrix = {}
                        # check if for this x_key under this y_key already a entry exist 
                        try:
                            entry = d_matrix[line[i_xaxis]]
                            sys.exit("Error: xy coordinate "+line[i_yaxis]+", "+line[i_xaxis]+" exist twice in this input. can not build dictionary of dictionary.")
                        except:         
                            d_matrix.update({line[i_xaxis] : line[i_value]})  # update row dictionary 
                            dd_matrix.update({line[i_yaxis] : d_matrix})  # update dictionary of row dictionaries
                    # else label or pre row
    # output
    print("o rdb2dd dd_matrix :", dd_matrix)            
    return(dd_matrix)


# bue 20140525: what about pyddl with b_ddl = True
def txtrdb2pydd(s_matrix, ivs_value, ivs_ykey, ivs_xkey, tivtsvivs_yvalue=None, tivtsvivs_xvalue=None, ivs_labelrow=None):
    """
    txtrdb2pydd - load txt file in rdb format into dictionary of dictionary 
    input: 
        s_matrix : string points to rdb input file.
        ivs_value : string or integer points to column holding the values. 0 is the first column  
        ivs_ykey : string or integer points to column holding the keys. 0 is the first column  
        ivs_xkey : string or integer points to column holding the keys. 0 is the first column
        tivtsvivs_yvalue : tuple of string or integer points to column holding the y axis annotation. default None is no y axis annotation. 0 is the first column
        tivtsvivs_xvalue : tuple of string or integer points to column holding the x axis annotation. default None is no x axis annotation. 0 is the first column
        ivs_labelrow : integer or string. default None is no label row. 0 is the first row 
    output: 
        dd_matrix : dictionary of dictionary with matrix content 
        dd_xaxis : dictionary of dictionary with x annotation content
        dd_yaxis : dictionary of dictionary with y annotation content
    """
    # input
    print("i txtrdb2pydd s_matrix :", s_matrix)
    print("i txtrdb2pydd ivs_value :", ivs_value)
    print("i txtrdb2pydd tivtsvivs_yvalue :", tivtsvivs_yvalue)
    print("i txtrdb2pydd ivs_ykey :", ivs_ykey)
    print("i txtrdb2pydd tivtsvivs_xvalue :", tivtsvivs_xvalue)
    print("i txtrdb2pydd ivs_xkey :", ivs_xkey)
    print("i txtrdb2pydd ivs_labelrow :", ivs_labelrow)
    # get integer coordinates 
    # ivs_labelrow == None case
    if (ivs_labelrow == None):
        if ( (type(ivs_ykey) != int) or (type(ivs_xkey) != int) or (type(ivs_value) != int) ):
            sys.exit("Error : if ivs_labelrow is None, ivs_ykey, ivs_xkey, and ivs_xkeyivs_xkey can not be anyting else then integer: "+str(ivs_labelrow)+", "+str(ivs_ykey)+", "+str(ivs_xkey)+", "+str(ivs_value)+".")
        else: 
            i_labelrow = ivs_labelrow  # label row, rdb2dd can handle None
            i_ykey = ivs_ykey  # y column
            i_xkey = ivs_xkey  # x column 
            i_value = ivs_value  # value
    # ivs_labelrow integer and string case
    else: 
        with open(s_matrix, newline='') as f:
            reader = csv.reader(f, delimiter='\t', quoting=csv.QUOTE_NONE) # read out inputfile
            for line in reader:
                #print("line :", str(reader.line_num), str(line))
                # handel line 
                line = linekicking(line)

                # ivs_labelrow string case 
                if  (type(ivs_labelrow) == str): 
                    # label row
                    try: 
                        line.index(ivs_labelrow) 
                        ivs_labelrow = reader.line_num - 1 
                    except: 
                        pass # nop    
                # ivs_labelrow integer and None case 
                if (type(ivs_labelrow) == int):
                    # find column label row
                    if ((reader.line_num - 1) == ivs_labelrow):
                        # hanle line 
                        i_labelrow = ivs_labelrow
                        # y column 
                        if (type(ivs_ykey) == int):
                            i_ykey = ivs_ykey
                        else:
                            i_ykey = line.index(ivs_ykey) 
                        # x column 
                        if (type(ivs_xkey) == int):
                            i_xkey = ivs_xkey
                        else:
                            i_xkey = line.index(ivs_xkey) 
                        # value
                        if (type(ivs_value) == int):
                            i_value = ivs_value
                        else:
                            i_value = line.index(ivs_value) 
                        # end part I
                        break 
            # end part II
            f.close()
    # get data 
    # matrix part
    dd_matrix = rdb2dd(s_matrix=s_matrix, i_yaxis=i_ykey, i_xaxis=i_xkey, i_value=i_value, i_labelrow=i_labelrow)
    # y part
    if (tivtsvivs_yvalue == None):
        dd_yaxis = None
    else:
        dd_yaxis = spre2dd(s_matrix=s_matrix, tivtsvivs_value=tivtsvivs_yvalue, i_ycoordinate=i_labelrow, i_xcoordinate=i_ykey)
    # x part
    if (tivtsvivs_xvalue == None):
        dd_xaxis = None
    else:
        dd_xaxis = spre2dd(s_matrix=s_matrix, tivtsvivs_value=tivtsvivs_xvalue, i_ycoordinate=i_labelrow, i_xcoordinate=i_xkey)
    # output
    print("o txtrdb2pydd dd_matrix :", dd_matrix)
    print("o txtrdb2pydd dd_yaxis :", dd_yaxis)
    print("o txtrdb2pydd dd_xaxis :", dd_xaxis)
    return(dd_matrix, dd_yaxis, dd_xaxis)



####################
# modues self test #
####################
if __name__=='__main__': 
    #### off shore modules ####
    # xaxislabelhandle
    print('\n*** selftest : x axis label handle (internal methode) 00 ***')
    xaxislabelhandle(l_xaxis=['abc','def','ghi','jklm','nop'], tivts_value=(3,4,), tivts_key=(0,1,2,))
    # xaxislabelhandle
    print('\n*** selftest : x axis label handle (internal methode) 01 ***')
    xaxislabelhandle(l_xaxis=['abc','def','ghi','jklm','nop'], tivts_value=(3,4,), tivts_key=(0,1,2,), tivtsvivs_primarykey=(0,1,))

    # linekicking
    print('\n*** selftest : line kicking (internal methode) ***')
    linekicking(['abc','a"b"c',"a'b'c",'   a b c   ', 'NA', 'NaN',])


    ### py lt 2 ###
    # pylt2extend 
    # bue 2014-05-12: make not much sense

    # pylt2pop
    print('\n*** selftest : py lt 2 pop ***')
    lt_matrix = [(1.1,1.2,1.3,), (2.1,2.2,2.3,), (3.1,3.2,None),]
    pylt2pop(lt_matrix, i_pop=1)
    
    # pylt2txtrdb
    lt_matrix = [('sa1', 'sb1', 'sample1', '1.1', '1.2', '1.3'), ('sa1', 'sb1', 'sample2', '2.1', '2.2', '2.3'), ('sa1', 'sb2', 'sample3', '3.1', '3.2', '3.3')]
    t_xaxis=('yannota', 'yannotb', 'tracks', 'gene1', 'gene2', 'gene3')
    print('\n*** selftest : py lt 2 txt rdb (internal methode) 00 ***')
    pylt2txtrdb(s_outputfile='selftestpylt2txtrdb00', lt_matrix=lt_matrix)
    print('\n*** selftest : py lt 2 txt rdb (internal methode) 01 ***')
    pylt2txtrdb(s_outputfile='selftestpylt2txtrdb01', lt_matrix=lt_matrix, t_xaxis=t_xaxis, s_mode='w')


    ### py dt 2 ###
    # pydt2pydtl
    print('\n*** selftest : py dt 2 py dtl ***')
    dt_matrix = {'yaxis1':(1.1,1.2,1.3,), 'yaxis2':(2.1,2.2,2.3,), 'yaxis3':(3.1,3.2,None)}
    pydt2pydtl(dt_matrix=dt_matrix)
    
    # pydt2pylt
    print('\n*** selftest : py dt 2 py lt ***')
    dt_matrix = {'yaxis1':(1.1,1.2,1.3,), 'yaxis2':(2.1,2.2,2.3,), 'yaxis3':(3.1,3.2,None)}
    pydt2pylt(dt_matrix=dt_matrix, lt_matrix=[])

    # pydt2extend
    print('\n*** selftest : py dt 2 extend ***')
    dt_matrix = {'yaxis1':(1.1,1.2,1.3,), 'yaxis2':(2.1,2.2,2.3,), 'yaxis3':(3.1,3.2,None)}
    dt_extend = {'yaxis1':(1.4,1.5,), 'yaxis2':(2.4,2.5,), 'yaxis3':(3.4,3.5,)}
    pydt2extend(dt_matrix, dt_extend)

    # pydt2pop
    print('\n*** selftest : py dt 2 pop ***')
    dt_matrix = {'yaxis1':(1.1,1.2,1.3,), 'yaxis2':(2.1,2.2,2.3,), 'yaxis3':(3.1,3.2,None)}
    pydt2pop(dt_matrix, i_pop=1)

    # pydt2txtrdb 
    dt_matrix = {'sa1_sb1_sample2': ('sa1', 'sb1', 'sample2', '2.1', '2.2', '2.3'),'sa1_sb2_sample3': ('sa1', 'sb2', 'sample3', '3.1', '3.2', '3.3'), 'sa1_sb1_sample1': ('sa1', 'sb1', 'sample1', '1.1', '1.2', '1.3')}
    t_xaxis=('primarykey','yannota', 'yannotb', 'tracks', 'gene1', 'gene2', 'gene3')
    print("\n*** selftest py dt 2 txt rdb 00 ***")
    pydt2txtrdb(s_outputfile='selftestpydt2txtrdb00', dt_matrix=dt_matrix)
    print("\n*** selftest py dt 2 txt rdb 01 ***")
    pydt2txtrdb(s_outputfile='selftestpydt2txtrdb01', dt_matrix=dt_matrix, t_xaxis=t_xaxis, s_mode='w')


    ### py dd 2 ###
    # pydd2transpose 
    print('\n*** selftest : py dd 2 transpose ***')
    dd_matrix = {'yaxis1':{'xaxis1':1.1, 'xaxis2':1.2, 'xaxis3':1.3}, 'yaxis2':{'xaxis1':2.1, 'xaxis2':2.2, 'xaxis3':2.3}, 'yaxis3':{'xaxis1':3.1, 'xaxis2':3.2, 'xaxis3':None}} 
    pydd2transpose(dd_matrix)

    # pydd2yxaxis
    print('\n*** selftest : py dd 2 yxaxis ***')
    dd_matrix = {'yaxis1':{'xaxis1':1.1, 'xaxis2':1.2, 'xaxis3':1.3}, 'yaxis2':{'xaxis1':2.1, 'xaxis2':2.2, 'xaxis3':2.3}, 'yaxis3':{'xaxis1':3.1, 'xaxis2':3.2, 'xaxis3':None}} 
    pydd2yxaxis(dd_matrix)

    # pydd2pydtrdb
    print('\n*** selftest : py dd 2 dt rdb ***')
    dd_matrix = {'yaxis1':{'xaxis1':1.1, 'xaxis2':1.2, 'xaxis3':1.3}, 'yaxis2':{'xaxis1':2.1, 'xaxis2':2.2, 'xaxis3':2.3}, 'yaxis3':{'xaxis1':3.1, 'xaxis2':3.2, 'xaxis3':None}} 
    pydd2pydtrdb(dd_matrix, dt_matrix={})

    # pydd2pydtspre
    print('\n*** selftest : py dd 2 dt spre ***')
    dd_matrix = {'yaxis1':{'xaxis1':1.1, 'xaxis2':1.2, 'xaxis3':1.3}, 'yaxis2':{'xaxis1':2.1, 'xaxis2':2.2, 'xaxis3':2.3}, 'yaxis3':{'xaxis1':3.1, 'xaxis2':3.2, 'xaxis3':None}} 
    pydd2pydtspre(dd_matrix, dt_matrix={})

    # pydd2txtrdb
    dd_matrix = {'yaxis1':{'xaxis1':1.1, 'xaxis2':1.2, 'xaxis3':1.3}, 'yaxis2':{'xaxis1':2.1, 'xaxis2':2.2, 'xaxis3':2.3}, 'yaxis3':{'xaxis1':3.1, 'xaxis2':3.2, 'xaxis3':None}} 
    dd_xaxis = {'ylabel1':{'xaxis1':'x_r1c1', 'xaxis2':'x_r1c2', 'xaxis3':'x_r1c3'}, 'ylabel2':{'xaxis1':'x_r2c1', 'xaxis2':'x_r2c2', 'xaxis3':'x_r2c3'}, 'ylabel3':{'xaxis1':'x_r3c1', 'xaxis2':'x_r3c2', 'xaxis3':'x_r3c3'},}
    dd_yaxis = {'yaxis1':{'ylabel1':'y_r1c1', 'ylabel2':'y_r1c2', 'ylabel3':'y_r1c3'},'yaxis2':{'ylabel2':'y_r2c2', 'ylabel1':'y_r2c1', 'ylabel3':'y_r2c3'},'yaxis3':{'ylabel1':'y_r3c1', 'ylabel2':'y_r3c2', 'ylabel3':'y_r3c3'},}
    print("\n*** selftest py dd 2 txt rdb 00 ***")
    pydd2txtrdb(s_outputfile='selftestpydd2txtrdb00', dd_matrix=dd_matrix,)
    print("\n*** selftest py dd 2 txt rdb 01 ***")
    pydd2txtrdb(s_outputfile='selftestpydd2txtrdb01', dd_matrix=dd_matrix, dd_xaxis=dd_xaxis, dd_yaxis=dd_yaxis)
 
    # pydd2txtspre
    dd_matrix = {'yaxis1':{'xaxis1':1.1, 'xaxis2':1.2, 'xaxis3':1.3}, 'yaxis2':{'xaxis1':2.1, 'xaxis2':2.2, 'xaxis3':2.3}, 'yaxis3':{'xaxis1':3.1, 'xaxis2':3.2, 'xaxis3':None}} 
    dd_xaxis = {'ylabel1':{'xaxis1':'x_r1c1', 'xaxis2':'x_r1c2', 'xaxis3':'x_r1c3'}, 'ylabel2':{'xaxis1':'x_r2c1', 'xaxis2':'x_r2c2', 'xaxis3':'x_r2c3'}, 'ylabel3':{'xaxis1':'x_r3c1', 'xaxis2':'x_r3c2', 'xaxis3':'x_r3c3'},}
    dd_yaxis = {'yaxis1':{'ylabel1':'y_r1c1', 'ylabel2':'y_r1c2', 'ylabel3':'y_r1c3'},'yaxis2':{'ylabel2':'y_r2c2', 'ylabel1':'y_r2c1', 'ylabel3':'y_r2c3'},'yaxis3':{'ylabel1':'y_r3c1', 'ylabel2':'y_r3c2', 'ylabel3':'y_r3c3'},}
    print("\n*** selftest py dd 2 txt spre 00 ***")
    pydd2txtspre(s_outputfile='selftestpydd2txtspre00', dd_matrix=dd_matrix,)
    print("\n*** selftest py dd 2 txt spre 01 ***")
    pydd2txtspre(s_outputfile='selftestpydd2txtspre01', dd_matrix=dd_matrix, dd_xaxis=dd_xaxis, dd_yaxis=dd_yaxis)


    ### txt spre 2 ###
    # spre2dd
    # txtspre2pydd 
    print("\n*** selftest : txt spre 2 py dd 00 ***")
    txtspre2pydd(s_matrix='selftestpydd2txtspre00_glue.txt')

    print("\n*** selftest : txt spre 2 py dd 01 ***")
    txtspre2pydd(s_matrix='spre.txt', tivs_yxaxis=(3,3,))
    print("\n*** selftest : txt spre 2 py dd 01y ***")
    txtspre2pydd(s_matrix='sprex.txt', tivs_yxaxis=(3,0,))  #!
    print("\n*** selftest : txt spre 2 py dd 01x ***")
    txtspre2pydd(s_matrix='sprey.txt', tivs_yxaxis=(0,3,))  #!

    print("\n*** selftest : txt spre 2 py dd 02 ***")
    txtspre2pydd(s_matrix='spre.txt', tivs_yxaxis='tracks')


    ### txt rdb 2 ###
    # txtrdb2pylt 
    print('\n*** selftest : txt rdb 2 py lt 00 ***')
    txtrdb2pylt (s_matrix='dos.txt', tivtsvivs_value=(0,1,2,3,4,5,))
    print('\n*** selftest : txt rdb 2 py lt 01 ***')
    txtrdb2pylt (s_matrix='dos.txt', tivtsvivs_value=(3,4,5,), tivtsvivs_key=(0,1,2,), ivs_labelrow=0)
    print('\n*** selftest : txt rdb 2 py lt 02 ***')
    txtrdb2pylt(s_matrix='dos.txt', tivtsvivs_value= ('gene1','gene2','gene3'), tivtsvivs_key=('yannota','yannotb','tracks'), ivs_labelrow='tracks')

    # rdb2dt
    # txtrdb2pydt 
    print("\n*** self test : txt rdb 2 py dt 00 ***")
    txtrdb2pydt(s_matrix='dos.txt', tivtsvivs_value=(3,4,5,), tivtsvivs_key=(0,1,2,), tivtsvivs_primarykey=(0,1,2,))
    print("\n*** self test : txt rdb 2 py dt 01 ***")
    txtrdb2pydt(s_matrix='dos.txt', tivtsvivs_value=(3,4,5,), tivtsvivs_key=(0,1,2,), tivtsvivs_primarykey=(0,1,2,), ivs_labelrow=0, b_dtl=False)
    print("\n*** self test : txt rdb 2 py dt 02 ***")
    txtrdb2pydt(s_matrix='dos.txt', tivtsvivs_value=(3,4,5,), tivtsvivs_key=(0,1,2,), tivtsvivs_primarykey=(0,1,), ivs_labelrow=None, b_dtl=True)
    print("\n*** self test : txt rdb 2 py dt 03 ***")
    txtrdb2pydt(s_matrix='dos.txt', tivtsvivs_value=(3,4,5,), tivtsvivs_key=(0,1,2,), tivtsvivs_primarykey=(0,1,), ivs_labelrow=0, b_dtl=True)
    print("\n*** self test : txt rdb 2 py dt 04 ***")
    txtrdb2pydt(s_matrix='dos.txt', tivtsvivs_value=('gene1','gene2','gene3',), tivtsvivs_key=('yannota','yannotb','tracks',), tivtsvivs_primarykey=('yannota','yannotb',), ivs_labelrow='tracks', b_dtl=True)

    # rdb2dd
    # txtrdb2pydd
    print("\n*** selftest : txt rdb 2 py dd 00 ***")
    txtrdb2pydd(s_matrix='rdb.txt', ivs_ykey=0, ivs_xkey=4, ivs_value=8) 
    print("\n*** selftest : txt rdb 2 py dd 01 ***")
    txtrdb2pydd(s_matrix='rdb.txt', tivtsvivs_yvalue=None,ivs_ykey=0, tivtsvivs_xvalue=None,ivs_xkey=4, ivs_value=8, ivs_labelrow=None) 
    print("\n*** selftest : txt rdb 2 py dd 02 ***") 
    txtrdb2pydd(s_matrix='rdb.txt', ivs_value='value', ivs_ykey='y_track', ivs_xkey='x_track', tivtsvivs_yvalue=('ylabel1','ylabel2','ylabel3',), tivtsvivs_xvalue=('xlabel1','xlabel2','xlabel3',), ivs_labelrow=0)
