"""
# file : glueto.py
#
# history:
#     author: bue
#     date: 2014-01-03
#     license: >= GPL3
#     language: Python 3.3.3
#     description: This library will provide  analysis modules, established in proximity of the glue libray.
#     though not each and every function is really dependent of the glue library, but most of the files will contain functions which are directly depending on the glue library.
#
# use:
#    cd /.../site-packages
#    ln -s  /.../src/gitorious/pyglueto/glueto.py
#    import pyglueto.glueto as glto
"""

# load bue standard object pyanalysis libraries
from pyanalysis.gltowell import *   # drug screen

# load plot library
from pyanalysis.gltoboxwhisker import *   # drug screen
from pyanalysis.gltocircos import *  # http://circos.ca and mycircos
from pyanalysis.gltocurve import *  # drug screen
from pyanalysis.gltoheattree import * # heatmap and phylogenetic tree
from pyanalysis.gltoscatter import *   # drug screen

# load bue standard object pygenometrack libraries
from pygenometrack.gltogene import *  # rdb gene and http://broadinstitute.org/igv/ track 
